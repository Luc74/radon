import numpy as np
from PIL  import Image
from glob import  glob
from skimage.io import imread
import matplotlib.pyplot as plt

# Prend un dossier remplpit d'image et renvoi des image filtrer enregistre dans un dossier 
# on enregistre seulement les 3 premiere image filtrer pour ne surrchager la memoire de l'ordinateur

class ReadData():
    
    ##constante
    SEUIL = 0.65#filtrage pour separer les forme et le blanc
    NORME = 255*np.sqrt(3)#Norme maximum pour filtrer
    DSIZE = (150,150)#nouvelle taille de l'image pour alleger le nombre de calvule a faire dans la classe a suivre
    
    ## atributs
    stock = None #tableau de stockage des image
    
    def __init__(self):
        path = glob('Data\dataset9\*') ## path est les image sctoké à cette endroit
        nbImage = len(path)
        self.stock = np.zeros((nbImage,self.DSIZE[0],self.DSIZE[1]))
        #pour l'image c'est (hauteur,largeur,RGB)
        for i in range(self.stock.shape[0]):## on remplit le tableau de stockage en redimentionnant et filtrant
            self.stock[i,:,:] = self.Seuillage(self.Resize(imread(path[i])))
            if i < 3  :  plt.imsave('./Result/imbrute/image%d.png'%i,self.stock[i,:,:])
        
            
   
    def Seuillage(self,tab): ##ameliorable, on seuille pour passer en binnaire 
        return(np.where(np.linalg.norm(tab,axis=2,keepdims=False)/self.NORME < self.SEUIL,0,np.linalg.norm(tab,axis=2,keepdims=False)).astype(float))
     
    def Resize(self,tab): #on prend un tableau que l'on interprette comme une image et qu'on redimentionne   
        tab = Image.fromarray(tab)
        return(np.array(tab.resize((self.DSIZE[1],self.DSIZE[0]))))
        
    
#test = ReadData()
 