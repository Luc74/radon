import numpy as np
from skimage.transform import iradon
import matplotlib.pyplot as plt
from transmi import ReadData
#from ReadData import ReadData
#from tesssst import ReadData

class InvRadon():
    
    #variable
    data = None## tableau contenant les image d'entre de la transformer
    D3Image = None##tableau contenant les image donner par la reconstruction des projection
    
    def __init__(self):
        self.data = ReadData()#on remplit le tableau avec les image traiter
        self.data.stock = np.transpose(self.data.stock,(2,0,1))#on modifie l'ordre des axes pour avoirs
        #le bonne ordre lors du découpage avec la transformer inverse de Radon
        ##(  longueur ligne = nb colonne,nb image = nb angle, nb plateau = nb ligne)
        for i in range(self.data.stock.shape[2]):
            plt.imsave('./Result/iradon2D/image%d.png'%i,iradon(np.squeeze(self.data.stock[:,:,i])))
            #On sauve les image apres quel est subit :
            #_Un decoupage pour avoir toutes les ligne d'une meme hauteur venant de toutes les projection
            #_On a tronquer la dernier dimention egale a 1 afin d'avoir un matrisse 2D et non un matrisse 3D style [:,:,1]
            #_On applique la transformer inverse de Radon
            #_on stock les image
        
#test = InvRadon()
