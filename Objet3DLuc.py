import numpy as np
from skimage.io import imread
from glob import  glob
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

class Objet3D():
    
    ##Attribue
     stock = None
        
     def __init__(self,imadresse,angle=320):
        path = glob(imadresse)
        nbImage = len(path)
        self.stock=np.zeros((nbImage,np.shape(imread(path[0]))[0],np.shape(imread(path[0]))[1],3))## 4 = 3 couleur + transparence
        ax = self.PrepAxe()
        for i in range(self.stock.shape[0]):
            print(imread(path[i]).shape)
            
            self.stock[i,:,:,:]=imread(path[i])[:,:,:3]
        self.stock=self.Norm(self.stock)
        print(self.stock.shape[0])
        x,y,z = np.indices([self.stock.shape[0] + 1,self.stock.shape[1] + 1,self.stock.shape[2] + 1])
        print(self.stock.shape,x.shape,y.shape,z.shape)
        ax.voxels(x[::10,::10,::10],y[::10,::10,::10],z[::10,::10,::10],
                  np.squeeze(self.stock[::10,::10,::10,0]))
        plt.show()
        
     def Norm(self,tab):
        return (tab-np.min(tab))/(np.max(tab)-np.min(tab))
    
     def PrepAxe(self,grille=False,angle=320):
         fig = plt.figure()
         ax = fig.gca(projection="3d")##gca prepare les axe si nessecaire icic oui pour la 3D
         ax.view_init(30,angle)## premier argu angle entre les paroit (axes en 2D) sui serve d'echelle pour l'impression de 3D
         # ax.set_xlim(right=self.stock[1])##on normalise la taille de la fenetre au cas on affiche une coupe ensuite
         # ax.set_ylim(top=self.stock[2])
         # ax.set_zlim(top=self.stock[0])
         ax.set_xlabel("x")
         ax.set_ylabel("y")
         ax.set_zlabel("z")
         ax.grid(grille)
         return ax

teste=Objet3D('./Result/iradon2D/*')