import numpy as np
from skimage.io import imread
from glob import  glob
import plotly.graph_objects as go
from plotly.offline import plot

#On prend un ensemble d'image stocker dans un dossier que l'on interprette
#comme un imge en nuence de gris et dont on imprime l'image 3D 
#qu'elle consititurais si elle formais des tranche de cette 3D

class Objet3D():
    
    ##Attribue
     stock = None#Tableau pour stocker les resultat de filtrage des image
        
     def __init__(self,imadresse):
        path = glob(imadresse)## path est l'ensemble des images sctoké à cette endroit
        nbImage = len(path)
        self.stock=np.zeros((nbImage,np.shape(imread(path[0]))[0],np.shape(imread(path[0]))[1]))
        #print(self.stock.shape)
        for i in range(self.stock.shape[0]):
            nom = './Result/iradon2D/image%d.png'%i#car ordre alphabetique != ordre des i
            self.stock[i,:,:]=imread(nom,as_gray=True)[:,:]
        # on calcule la norme car les couleur on été disposer
        #par la transformer de randon selon une color map et ne represente donc aucune realiter    
        self.stock=self.Norm(self.stock)
        # on prend que une image sur 5 car sinon le nombre de donne est trop important pour etre traiter
        self.stock = self.stock[::5,::5,::5]
        #on construit nos axes, on commence à 0, on s'arrette an nombre d'image d'ou le self.stock.shape[0]
        #puis on fait autant de "pas" que on a sauvgarder d'image soit 150/5 = 30 et  j et l'indicatuer que c'est le pas
        x,y,z = np.mgrid[0:self.stock.shape[0]:30j,0:self.stock.shape[1]:30j,0:self.stock.shape[2]:30j]
        #on met en place la figure 3D
        fig = go.Figure(data=go.Volume(
        #mise en place des axes
        x=x.flatten(),
        y=y.flatten(),
        z=z.flatten(),
        #on donne les valeur
        value=self.stock.flatten(),
        #on indique la plage de valeur a afficher, en filtrant pour suprimer le bruit
        isomin=0.4,#0.35 pour les triangles
        isomax=1,
        opacity=0.2, # transmitance en fonction des valeurs, doit etre faible pour que l'on voit l'interieure
        surface_count=59, # needs to be a large number for good volume rendering
        ))
        plot(fig)#on affiche la figure
        
        
     def Norm(self,tab):#on norme les intensiter lumineuse
        return (tab-np.min(tab))/(np.max(tab)-np.min(tab))
    


#teste=Objet3D('./Result/iradon2D/*')