"""
Luc Vandame, projet avec Lois Allaire
vandameluc@gmail.com
Debut de programation des classes : 17/09/2020
Derniere modification : 14/01/2020
TIPE
Reconsrtuction d'un objet en 3D a partir de photos

libraries used :
    numpy
    matplotlib.pyplot
    skimage.io
    PIL
    glob
    plotly.graph_objects
    plotly.offline

ordre d'exution des classes :
    class ReadData de ReadData ou transmi ou de tesssst(ReadData a privilegier pour les objets pleins sinon transmi 
    ; tesssst ne sert qu'a regerder l'effet du filtrage soit une utiliter uniquement pour les comparaison)
    class InvRadon de Invradon
    class Objet3D de Objet3D

particularite pour le fonctionnement:
    tous les dossiers indiques doivent etre present a partir du lieu de stockage des codes
    les photo doivent etre stocke dans un fichier dont l'addresse doit etre indiquer dans la class
    ReadData utiliser et sous format JPG
    un fichier Result doit etre cree, il doit contenir deux sous dossier : imbrut et image
    
todo :
    amelioration du filtrage
    augmenter la qualite de reduction d'image
    ameliorer la bibliotheque graphique pour avoir une meilleure resolution dans la reconstruction

note : le chronometrage est juste a titre informatif
"""

from InvRadon import InvRadon
from Objet3D import Objet3D
from time import perf_counter as pc

deb = pc()
print("debut")
test = InvRadon()
print("invradon ok")
teste=Objet3D('./Result/iradon2D/*')
print("fin")
fin = pc()
print(fin-deb)